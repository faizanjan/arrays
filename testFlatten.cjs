#!/usr/bin/env node
const flatten = require("./flatten.cjs")

const nestedArray = [1, [2], [[3,4,5]], [[[6],7],8,9],10];
// const nestedArray = [1, [2], [[3]], [[[4]]]]; 

console.log(flatten(nestedArray,2))
console.log(nestedArray.flat(2))