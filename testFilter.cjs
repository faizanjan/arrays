const filter = require("./filter.cjs");

const items = [1, 2, '3', 4, 50, 5, 'abc'];
// const cb = el=> el%2===0;
const cb = (el, i, arr)=> typeof(el)==='string';

const result = filter(items, cb);
console.log(result);