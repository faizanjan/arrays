const flatten = (arr, depth = 1) => {
    if (arr === undefined || !Array.isArray(arr)) {
      return [];
    }
    if (depth === 0) {
      return arr.slice();
    }
    let ans = [];
    for (let index = 0; index < arr.length; index++) {
      if (Array.isArray(arr[index])) {
        ans.push(...flatten(arr[index], depth - 1));
      } else if (arr[index] !== undefined) {
        ans.push(arr[index]);
      }
    }
    return ans;
  };
  

module.exports = flatten