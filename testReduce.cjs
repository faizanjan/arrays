const reduce = require("./reduce.cjs");

const items = [1, 2, 3, 4, 5, 5];
const cb = (acc,el)=>acc+=el;

const result = reduce(items, cb);

if(result) console.log(result);
else console.log("Pass proper arguments");