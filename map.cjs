
function mapObject(array, callback) {
    //check for argument validation
    if(!Array.isArray(array)||
        !callback || !array ||
        typeof callback!== 'function'
    ){
        return [];
    }

    let newArray = [];
    for(let index=0; index<array.length; index++) {
            let newElement = callback(array[index], index, array);
        newArray.push(newElement);
    }
    return newArray;
}

module.exports = mapObject