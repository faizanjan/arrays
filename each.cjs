const each = (array, callback)=>{

    //check for argument validation
    if(!Array.isArray(array)||
        !callback || !array ||
        typeof callback!== 'function'
    ) {
        console.log("Pass proper arguments");
        return;
    }

    for(let index=0; index<array.length; index++) {
        callback(array[index], index, array);
    }
}

module.exports = each;