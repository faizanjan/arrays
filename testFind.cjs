const find = require("./find.cjs");

const items = [1, 2, 3, 4, 5, 5];
const cb = el=> el%2===0;
// const cb = el=> typeof(el)==='string';

const result = find(items, cb);
console.log(result);