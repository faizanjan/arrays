
function find(array, callback) {
    //check for argument validation
    if(!Array.isArray(array)||
        !callback || !array ||
        typeof callback!== 'function'
    ) return;

    for(let index=0; index<array.length; index++) {
        if (callback(array[index])) return array[index];
    }
    return;
}

module.exports = find