
function reduce(array, callback, startingValue) {
    //check for argument validation
    if(!Array.isArray(array)||
        !callback || !array ||
        typeof callback!== 'function'
    ){
        return;
    }

    let si = 0;
    let accumulator = startingValue===undefined? array[si++] : startingValue;
    for(let index=si; index<array.length; index++){
        accumulator = callback(accumulator,array[index], index, array)
    }
    return accumulator;
}

module.exports = reduce