
function filter(array, callback) {
    //check for argument validation
    if(!Array.isArray(array)||
        !callback || !array ||
        typeof callback!== 'function'
    ) return [];

    const filteredArr = [];
    for(let index=0; index<array.length; index++) {
        if (callback(array[index], index, array)===true) filteredArr.push(array[index]);
    }
    return filteredArr;
}

module.exports = filter